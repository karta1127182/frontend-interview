import axios, { Axios } from 'axios'
import qs from 'qs'

let LINE_CHANELL_ID="1657277784"
let LINE_REDIRECT_URI="http://localhost:3000/callback"
let LINE_CHANELL_SECRET="8d8eb7a52f0857696274b7d4afdc901b"
export const GetLineAuthLink = async () => {
  let url="https://access.line.me/oauth2/v2.1/authorize?"
      url+="response_type=code"
      url+=`&client_id=${LINE_CHANELL_ID}`
      url+=`&redirect_uri=${LINE_REDIRECT_URI}`
      url+="&state=123456789"
      url+="&scope=profile%20openid"
      url+="&nonce=12345673"
  return url
}

export const GetLineAccessToken = async (code) => {
  let options=qs.stringify({
    code:code,
    grant_type:'authorization_code',
    redirect_uri:LINE_REDIRECT_URI,
    client_id:LINE_CHANELL_ID,
    client_secret:LINE_CHANELL_SECRET
  })
  const res= await axios.post('https://api.line.me/oauth2/v2.1/token',options,{headers:{'Content-Type': 'application/x-www-form-urlencoded'}})
  return res
}

export const GetLineMeProfile = async (accessToken) => {
  let options=qs.stringify({
    client_id:LINE_CHANELL_ID,
    id_token:accessToken
  })
  const me= await axios.post('https://api.line.me/oauth2/v2.1/verify',options,{headers:{'Content-Type': 'application/x-www-form-urlencoded'}})
  return me
}
